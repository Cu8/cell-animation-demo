import "./index.css";

import {
  resizeCanvas,
  createGameLoop,
  createBatch,
  createOrthoCamera,
  InputHandler,
  fillRect,
  Vector2,
  drawPolygon
} from "gdxjs";
import createWhiteTex from "gl-white-texture";
import Board from "./Board";

const CELL_COLORS = [
  [1, 1, 1],
  [1, 0, 0],
  [0, 1, 0],
  [0, 0, 1]
];
const EMPTY_COLOR = [0, 0, 0];

const canvas = document.getElementById("main");
const [width, height] = resizeCanvas(canvas);
const gl = canvas.getContext("webgl");

const inputHandler = new InputHandler(canvas);

const BOARD_WIDTH = 6;
const BOARD_HEIGHT = 6;

const CELL_SIZE = (width * 0.8) / BOARD_WIDTH;
const START_X = width * 0.1;
const START_Y = (height - CELL_SIZE * BOARD_HEIGHT) / 2;

const batch = createBatch(gl);
const camera = createOrthoCamera(width, height, width, height);
const whiteTex = createWhiteTex(gl);

const board = new Board(BOARD_WIDTH, BOARD_HEIGHT);
board.randomize();

const DEVICE_PIXEL_RATIO = window.devicePixelRatio || 1;

const touched = [];

const checkTouched = (x, y) => {
  x = DEVICE_PIXEL_RATIO * x;
  y = DEVICE_PIXEL_RATIO * y;
  screenCoords.set(x, y);
  camera.unprojectVector2(worldCoords, screenCoords);
  worldCoords.sub(START_X, START_Y).scale(1 / CELL_SIZE, 1 / CELL_SIZE);
  const touchedX = Math.floor(worldCoords.x);
  const touchedY = Math.floor(worldCoords.y);
  const pos = board.getPosByXY(touchedX, touchedY);
  touched[pos] = true;
};

const FALLING_SPEED = 6;

const cellOffsets = [];

const tmpOffset = [];
const fillCell = () => {
  for (let x = 0; x < board.width; x++) {
    let currentOffset = 0;
    tmpOffset.length = 0;
    for (let y = board.height - 1; y >= 0; y--) {
      if (board.getCellByXY(x, y) === undefined) {
        currentOffset++;
      }
      tmpOffset[y] = currentOffset;
    }
    for (let y = board.height - 1; y >= 0; y--) {
      const cellState = board.getCellByXY(x, y);
      if (cellState !== undefined) {
        const newY = y + tmpOffset[y];
        const oldPos = board.getPosByXY(x, y);
        const newPos = board.getPosByXY(x, newY);
        board.state[oldPos] = undefined;
        board.state[newPos] = cellState;
        cellOffsets[newPos] = tmpOffset[y];
      }
    }
    for (let i = 0; i < currentOffset; i++) {
      const pos = board.getPosByXY(x, i);
      board.state[pos] = Math.floor(Math.random() * 4);
      cellOffsets[pos] = currentOffset;
    }
  }
};

const deleteTouchedCell = () => {
  for (let i = 0; i < touched.length; i++) {
    if (touched[i]) {
      board.state[i] = undefined;
    }
  }
  fillCell();
  console.log(cellOffsets);
};

const screenCoords = new Vector2();
const worldCoords = new Vector2();
inputHandler.addEventListener("touchStart", (x, y) => {
  touched.length = 0;
  checkTouched(x, y);
});

inputHandler.addEventListener("touchMove", (x, y) => {
  checkTouched(x, y);
});

inputHandler.addEventListener("touchEnd", () => {
  deleteTouchedCell();
  touched.length = 0;
});

gl.clearColor(0, 0, 0, 1);
const verts = [];
const update = delta => {
  for (let i = 0; i < cellOffsets.length; i++) {
    if (cellOffsets[i] === undefined || cellOffsets[i] <= 0) {
      cellOffsets[i] = 0;
      continue;
    }
    cellOffsets[i] -= FALLING_SPEED * delta;
  }
  gl.clear(gl.COLOR_BUFFER_BIT);
  batch.setProjection(camera.combined);
  batch.begin();
  for (let y = 0; y < board.height; y++) {
    for (let x = 0; x < board.width; x++) {
      const type = board.getCellByXY(x, y);
      const color = CELL_COLORS[type] || EMPTY_COLOR;
      const pos = board.getPosByXY(x, y);
      let drawY = START_Y + y * CELL_SIZE + 0.1 * CELL_SIZE;
      if (cellOffsets[pos] && cellOffsets[pos] > 0) {
        drawY -= cellOffsets[pos] * CELL_SIZE;
      }
      batch.setColor(...color, 1);
      fillRect(
        batch,
        whiteTex,
        START_X + x * CELL_SIZE + 0.1 * CELL_SIZE,
        drawY,
        CELL_SIZE * 0.8,
        CELL_SIZE * 0.8
      );
      batch.setColor(1, 1, 1, 1);
      if (touched[board.getPosByXY(x, y)]) {
        verts.length = 0;
        verts.push(START_X + x * CELL_SIZE, START_Y + y * CELL_SIZE);
        verts.push(
          START_X + x * CELL_SIZE,
          START_Y + y * CELL_SIZE + CELL_SIZE
        );
        verts.push(
          START_X + x * CELL_SIZE + CELL_SIZE,
          START_Y + y * CELL_SIZE + CELL_SIZE
        );
        verts.push(
          START_X + x * CELL_SIZE + CELL_SIZE,
          START_Y + y * CELL_SIZE
        );
        drawPolygon(batch, whiteTex, verts, 10, 0, 1, 0);
      }
    }
  }
  batch.end();
};

createGameLoop(update);
