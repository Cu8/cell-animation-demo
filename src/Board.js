export default class Board {
  state = [];
  width = 6;
  height = 6;
  constructor(width, height) {
    this.width = width;
    this.height = height;
  }
  randomize() {
    for (let i = 0; i < this.width * this.height; i++) {
      this.state[i] = Math.floor(Math.random() * 4);
    }
  }
  getCell(pos) {
    return this.state[pos];
  }
  getPosByXY(x, y) {
    return y * this.width + x;
  }
  getCellByXY(x, y) {
    return this.state[this.getPosByXY(x, y)];
  }
}
